\babel@toc {polish}{}
\contentsline {part}{\numberline {I\relax .\leavevmode@ifvmode \kern .5em }Projekt}{2}{part.1}% 
\contentsline {chapter}{\numberline {1\relax .\leavevmode@ifvmode \kern .5em }Opis zadania}{3}{chapter.1}% 
\contentsline {part}{\numberline {II\relax .\leavevmode@ifvmode \kern .5em }Klasyfikacja losowego obrazu z ka\.zdej klasy}{4}{part.2}% 
\contentsline {chapter}{\numberline {2\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki podczas klasyfikacji obraz\'ow z u\.zyciem sieci neuronowej GoogLeNet}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1\relax .\leavevmode@ifvmode \kern .5em }Rezultaty dla flagi USA}{5}{section.2.1}% 
\contentsline {section}{\numberline {2.2\relax .\leavevmode@ifvmode \kern .5em }Rezultaty dla flagi Wielkiej Brytani}{7}{section.2.2}% 
\contentsline {section}{\numberline {2.3\relax .\leavevmode@ifvmode \kern .5em }Rezultaty dla flagi Korei Po\IeC {\l }udniowej}{8}{section.2.3}% 
\contentsline {section}{\numberline {2.4\relax .\leavevmode@ifvmode \kern .5em }Rezultaty dla flagi Brazyli}{11}{section.2.4}% 
\contentsline {section}{\numberline {2.5\relax .\leavevmode@ifvmode \kern .5em }Wnioski z klasyfikacji pojedynczych obrazk\'ow}{11}{section.2.5}% 
\contentsline {part}{\numberline {III\relax .\leavevmode@ifvmode \kern .5em }Douczenie sieci neuronowej GoogLeNet}{13}{part.3}% 
\contentsline {chapter}{\numberline {3\relax .\leavevmode@ifvmode \kern .5em }Przebieg douczania sieci neuronowej GoogLeNet}{14}{chapter.3}% 
\contentsline {chapter}{\numberline {4\relax .\leavevmode@ifvmode \kern .5em }Uzyskane wyniki podczas klasyfikacji obraz\'ow z u\.zyciem sieci neuronowej GoogLeNet}{15}{chapter.4}% 
\contentsline {section}{\numberline {4.1\relax .\leavevmode@ifvmode \kern .5em }Pierwsza pr\'oba}{15}{section.4.1}% 
\contentsline {section}{\numberline {4.2\relax .\leavevmode@ifvmode \kern .5em }Druga pr\'oba}{16}{section.4.2}% 
\contentsline {section}{\numberline {4.3\relax .\leavevmode@ifvmode \kern .5em }Trzecia pr\'oba}{17}{section.4.3}% 
\contentsline {chapter}{\numberline {5\relax .\leavevmode@ifvmode \kern .5em }Zamro\.zenie wag w pocz\k atkowych warstwach}{19}{chapter.5}% 
\contentsline {chapter}{\numberline {6\relax .\leavevmode@ifvmode \kern .5em }Mo\.zliwo\'s\'c przyspieszenia uczenia dzi\k eki wykorzystaniu GPU lub wielu procesor\'ow}{21}{chapter.6}% 
