%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Classify with GoogLeNet    %
%        Marcin Dolicher        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear; 
close all

net = googlenet;

%% Resize image 
I = imread('..\Projekt 3\Obrazy\USA\Usa_13.jpg');
% figure
% imshow(I)
sizeImage = size(I);
I = imresize(I,[224 224]);
figure
imshow(I)

%% Classify image
[label,scores] = classify(net,I);
label

%% Show results
figure
imshow(I)

[~,idx] = sort(scores,'descend');
idx = idx(5:-1:1);
classNamesTop = net.Layers(end).ClassNames(idx);
scoresTop = scores(idx);

title(string(label) + ", "  + num2str(100*scoresTop(1,5)) + "%");

figure
barh(scoresTop)
xlim([0 1])
title('Top 5 Predictions')
xlabel('Probability')
yticklabels(classNamesTop)